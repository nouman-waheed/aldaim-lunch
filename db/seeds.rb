# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Item.create(name: 'Dhaleem', price: 50)
Item.create(name: 'Chicken', price: 50)
Item.create(name: 'Aloo Qeema', price: 80)
Item.create(name: 'Daal Chawal', price: 60)
Item.create(name: 'Dahi Bhalay', price: 60)
Item.create(name: 'Fruit Chat', price: 70)
Item.create(name: 'Careem Chat', price: 70)
Item.create(name: 'Shami Tikki', price: 20)
Item.create(name: 'Besan Naan', price: 40)
Item.create(name: 'Bong Paye', price: 100)
Item.create(name: 'Chany', price: 30)
Item.create(name: 'Chany Ki Daal', price: 50)
Item.create(name: 'Daal Mash', price: 50)
Item.create(name: 'Lobia', price: 50)
Item.create(name: 'Mix Sabzi', price: 50)
Item.create(name: 'Sabzi', price: 50)
Item.create(name: 'Kari Pakora', price: 50)
Item.create(name: 'Samosa', price: 20)
Item.create(name: 'Aloo Naan', price: 30)
Item.create(name: 'Aloo Anda', price: 50)
Item.create(name: 'Nehari', price: 100)
Item.create(name: 'Aloo Paratha', price: 30)
Item.create(name: 'Biryani', price: 100)

