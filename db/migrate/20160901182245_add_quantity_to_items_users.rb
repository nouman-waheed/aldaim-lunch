class AddQuantityToItemsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :items_users, :quantity, :int
  end
end
