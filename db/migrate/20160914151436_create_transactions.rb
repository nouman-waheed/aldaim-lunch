class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
    	t.column "user_id", :int
    	t.column "action", :string
    	t.column "amount", :int
    	t.column "description", :string
    	t.column "balance", :int
    	t.column "time_stamp", :string
    end
  end
end