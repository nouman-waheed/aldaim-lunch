class AddDaysToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :day, :string
  end
end
