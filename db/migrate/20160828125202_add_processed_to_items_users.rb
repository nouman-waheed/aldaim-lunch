class AddProcessedToItemsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :items_users, :processed, :string
  end
end
