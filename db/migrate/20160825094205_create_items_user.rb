class CreateItemsUser < ActiveRecord::Migration[5.0]
  def up
    create_table :items_users do |t|
      t.integer "user_id"
      t.integer "item_id"
      t.timestamp "time_stamp" 
    end
    add_index(:items_users, ["user_id","item_id"])
  end
  def down
    drop_table :items_users
  end
end
