class CreateItems < ActiveRecord::Migration[5.0]
  def up
    create_table :items do |t|
    	t.column "name", :string, :limit => 50
    	t.column "price", :int
      t.timestamps
    end
  end
  def down
  	drop_table :items;
  end
end