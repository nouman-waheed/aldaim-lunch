class AddSubstituteToItemsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :items_users, :substitute, :string
  end
end
