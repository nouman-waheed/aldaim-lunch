require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mytest
  class Application < Rails::Application

    config.time_zone = 'Asia/Karachi'
    config.active_record.default_timezone = 'Asia/Karachi'
    Time.zone = 'Asia/Karachi'

  end
end
