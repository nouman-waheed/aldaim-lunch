Rails.application.routes.draw do

  get 'terms_and_provider/term'=> 'terms_and_provider#term'
  get 'terms_and_provider/privacy'
  get 'terms_and_provider/provider'
  get 'item/order'
  get 'item/history'
  get 'item/transactions'
  get 'admin/history'
  get 'admin/index'
  get 'admin/funds'
  get 'admin/item'
  get "add_order" => redirect { |params, request| "item/add_order?#{request.params.to_query}" }
  # devise_for :users , controllers: {omniauth_callbacks: "users/omniauth_callbacks"}
  devise_for :users
  
  match ':controller(/:action(/:id))', :via => [:get, :post, :patch]
  match ':controller(/:action)', :via => [:get, :post, :patch]
  root "item#order"

end
