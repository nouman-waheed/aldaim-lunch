class Admin < ActiveRecord::Base
  
  validates_presence_of :name, :price
  validates_length_of :name, :within => 3..20

end