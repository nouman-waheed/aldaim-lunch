class Item < ApplicationRecord

	validates_presence_of :name, :price, :cat , :day
	validates_length_of :name, :within => 3..20
	validates_length_of :price, :within => 1..3


	has_and_belongs_to_many :user
	scope :sorted, lambda {order("items.name ASC")}
end
