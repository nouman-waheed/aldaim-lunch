class User < ActiveRecord::Base
  
  # validates_presence_of :email, :password
  # validates_length_of :email,:password, :within => 3..5

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "24x24#" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  # belongs_to :item
  has_and_belongs_to_many :items

  # devise :omniauthable, :omniauth_providers => [:twitter,:facebook,:google_oauth2,:github,:linkedin]

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  def self.from_omniauth(auth)
    # byebug
  where(provider: auth.provider, u_id: auth.uid).first_or_create do |user|
    user.email = auth.info.email
    user.password = Devise.friendly_token[0,20]
    user.name = auth.info.name   # assuming the user user has a name
    # user.image = auth.info.image # assuming the user user has an image
  end
end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.data"] && session["devise.data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
        user.password = data["password"] if user.password.blank?
        user.name = data["name"] if user.name.blank?
      end
    end
  end
  
end
