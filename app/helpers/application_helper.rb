module ApplicationHelper


	def error_messages_for(object)
		render(:partial => "application/error_messages", :locals => {:object => object})
	end
	
	def icon_tag(integer)
		
		if integer < 10
			images = []
			images << image_tag("0.png")
			images << image_tag("#{integer.to_s[0]}.png")
			content_tag :div, images.join("\n").html_safe
		elsif integer < 100
			images = []
			images << image_tag("#{integer.to_s[0]}.png")
			images << image_tag("#{integer.to_s[1]}.png")
			content_tag :div, images.join("\n").html_safe
		elsif integer < 1000
			images = []
			images << image_tag("#{integer.to_s[0]}.png")
			images << image_tag("#{integer.to_s[1]}.png")
			content_tag :div, images.join("\n").html_safe
		end
		
	end
end
