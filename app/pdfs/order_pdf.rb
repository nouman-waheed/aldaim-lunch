class OrderPdf < Prawn::Document
	def initialize(orders)
		super()
		@orders = orders
		date = Date.today.strftime("%d %B %Y , %A ")
		text  "Aldaim Lunch - Orders List - #{date}"
		text  "\n"
		

		@user = nil
        @orders.each do |list|
            if @user != list["user_id"]
                @user = list["user_id"]
                text  "\n"
                text User.find(@user).name
                order
    		end
    	end
		
	end

	def order
		@item = Array.new
		@item.push ["Primary","Price","Quantity","Substitute","Price","Quantity"]

		@orders.each do |order|
            if order["user_id"] == @user && order["substitute"] == nil
            	primary = Item.find(order["item_id"]).name
            	price = Item.find(order["item_id"]).price
            	quantity = order["quantity"]
            	

            	search = "%#{order['id']}%" 
				item = ItemsUser.where('substitute::text LIKE :substitute', :substitute => search) 
				if  item[0] != nil
					details = Item.find(item[0].item_id)
					substitute = details.name
	            	sprice = details.price
	            	squantity = item[0]["quantity"]
				end


            	@item.push [primary,price,quantity,substitute,sprice,squantity]
            end
        end

		table order_all do
			self.row_colors = ["DDDDDD","FFFFFF"]
			self.header = true
		end
	end

	def order_all
		@item
	end
end