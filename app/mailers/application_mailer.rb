class ApplicationMailer < ActionMailer::Base
  default from: 'numan.waheed.aldaim@gmail.com'
  layout 'mailer'
end
