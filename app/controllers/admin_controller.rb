class AdminController < ApplicationController
before_action :authenticate_user!, :except => [:verify]
skip_before_action :verify_authenticity_token
before_action :verify

  def verify
    if current_user.role == nil
      sign_out_and_redirect(current_user)
    end
  end

  def index
    @items = Item.sorted    

    date = Date.today.to_s
    sql = "Select * from items INNER JOIN items_users on items.id = items_users.item_id where items_users.time_stamp > '#{date}' order by items_users.user_id ASC" 
    # byebug
    connection = ActiveRecord::Base.connection()
    test = connection.execute(sql)
    @data = test.collect{|t| t}


    respond_to do |format|
      format.html
      format.pdf do
        pdf = OrderPdf.new(@data)
        send_data pdf.render,filename: "order.pdf",type: 'application/pdf', disposition: 'inline'
      end
    end
    # byebug
  end

  def history
    #byebug
    @items = Item.sorted    

    date = Date.today.to_s
    sql = "Select * from items INNER JOIN items_users on items.id = items_users.item_id where items_users.time_stamp < '#{date}' ORDER BY items_users.id DESC" 
    
    connection = ActiveRecord::Base.connection()
    test = connection.execute(sql)
    @data = test.collect{|t| t}
    # byebug
  end

  def process_order
 
    subitem = ItemsUser.find(params[:id])

    if subitem.substitute == nil
      item = ItemsUser.where("substitute::text LIKE :substitute", :substitute => "%#{subitem.id}%")
      if item[0] != nil
        a = item[0].substitute.gsub("#{subitem.id},", "")
        if a == ""
          item[0].destroy
        else
          item[0].update_column :substitute, a
          item[0].save
        end
      end
    else
      slist = subitem.substitute.split(",")
      
      slist.each do |list|
        if list != ""
          item = ItemsUser.where("id::text LIKE :substitute", :substitute => "%#{list}%")
          if item[0] != nil
            subitem.update_column :substitute, nil
            item[0].destroy
          end
        end
      end

    end



    subitem.update_column :processed, 'yes'

    user = User.find(subitem.user_id)
    item = Item.find(subitem.item_id)

    transaction = Transaction.create("user_id" => subitem.user_id ,"action" => "deduct","amount" => (item.price*subitem.quantity.to_i),"description" => "order processed successfully.","balance" => user.balance,"time_stamp" => DateTime.now.strftime("%Y-%m-%d %H:%M:%S"))

    user.update_column :balance, user.balance-(item.price*subitem.quantity.to_i)



    if subitem.save && user.save && transaction.save
      flash[:notice] = "Order processed successfully!"
      redirect_to(:action => "index")
    end
  end

  def discard_order
    subitem = ItemsUser.find(params[:id])

    item = ItemsUser.where("substitute LIKE :substitute", :substitute => "%#{subitem.id}%")
    


    if item[0] != nil
      a = item[0].substitute.gsub("#{subitem.id},", "")
      if a == ""
        item[0].update_column :substitute, nil
      else
        item[0].update_column :substitute, a
      end
      
      item[0].save

    end


    if subitem.destroy
      flash[:notice] = "Order discarded successfully!"
      redirect_to(:action => "index")
    end
  end

  def funds
    #byebug
    @users = User.where('role' => nil).order("id ASC")
    # byebug
  end

  def add_funds
    # byebug

    if params[:commit] == "Add"
      user = User.find(params[:id])

      if user.balance == nil
        transaction = Transaction.create("user_id" => user.id ,"action" => "add","amount" => params[:balance].to_i,"description" => "funds added successfully.","balance" => user.balance,"time_stamp" => DateTime.now.strftime("%Y-%m-%d %H:%M:%S"))
        user.update_column :balance, params[:balance].to_i
      else
        transaction = Transaction.create("user_id" => user.id ,"action" => "add","amount" => params[:balance].to_i,"description" => "funds added successfully.","balance" => user.balance,"time_stamp" => DateTime.now.strftime("%Y-%m-%d %H:%M:%S"))
        user.update_column :balance, user.balance+params[:balance].to_i
      end



      if user.save && transaction.save
        flash[:notice] = "Funds updated with \"#{params[:balance]}\" successfully!"
        redirect_to(:action => "funds")
      end
    else
      user = User.find(params[:id])

      transaction = Transaction.create("user_id" => user.id ,"action" => "subtract","amount" => params[:balance].to_i,"description" => "funds subtracted successfully.","balance" => user.balance,"time_stamp" => DateTime.now.strftime("%Y-%m-%d %H:%M:%S"))

      user.update_column :balance, user.balance-params[:balance].to_i
      
      if user.save && transaction.save
        flash[:notice] = "Funds rolled back with \"#{params[:balance]}\" successfully!"
        redirect_to(:action => "funds")
      end
    end

  end


  def item
    @items = Item.all
  end

  def new
    @item = Item.new
  end

  def create
    #byebug
    @item = Item.new(item_params)
    @item.day = params[:sub]
    if @item.save
      redirect_to(:action => "item")
      flash[:notice] = "Item Added Successfully!";
    else
      render("new")
    end
  end

  def edit
    @item = Item.find(params[:id])
  end

  def delete
    @item = Item.find(params[:id])
  end

  def update
    @item = Item.find(params[:id])

    if @item.update_attributes(item_params)
      
      @item.update_column :day, params[:sub]
      @item.save

      redirect_to(:action => "item")
      flash[:notice] = "Item Details Updated!";
    else
      render("edit")
    end
  end

  def destroy
    @item = Item.find(params[:id])
    if @item.destroy
      redirect_to(:action => "item")
      flash[:notice] = "Item \"#{@item.name}\" Deleted!";
    else
      render("delete")
    end
  end

  private
    def item_params
      params.require(:item).permit(:name,:price,:cat,:day)
    end

end
