class ItemController < ApplicationController
before_action :authenticate_user!, :except => [:verify]
skip_before_action :verify_authenticity_token
before_action :verify

  def verify
    if current_user.role == "admin"
      sign_out_and_redirect(current_user)
    end
  end


  def order
    day = Date.today.strftime("%a").downcase
    day = "%#{day}%"
    # byebug
    @items = Item.sorted
    @cat_fastfood = Item.where("cat::text LIKE :cat AND day::text LIKE :day", :cat => "%fastfood%",:day => day).order("items.name ASC")
    @cat_sweet = Item.where("cat::text LIKE :cat AND day::text LIKE :day", :cat => "%sweet%",:day => day).order("items.name ASC")
    @cat_drink = Item.where("cat::text LIKE :cat AND day::text LIKE :day", :cat => "%drink%",:day => day).order("items.name ASC")
    @cat_salan = Item.where("cat::text LIKE :cat AND day::text LIKE :day", :cat => "%salan%",:day => day).order("items.name ASC")
    @cat_bread = Item.where("cat::text LIKE :cat AND day::text LIKE :day", :cat => "%bread%",:day => day).order("items.name ASC")


    date = Date.today.to_s
    sql = "Select * from items INNER JOIN items_users on items.id = items_users.item_id where items_users.user_id = #{current_user.id} AND items_users.time_stamp >= '#{date}' ORDER BY items_users.id DESC" 
    # byebug
    connection = ActiveRecord::Base.connection()
    test = connection.execute(sql)
    @data = test.collect{|t| t}
    # byebug
  end

  def add_order
    
    date = Date.today.to_s
    sql = "Select * from items INNER JOIN items_users on items.id = items_users.item_id where items_users.user_id = #{current_user.id} AND items_users.time_stamp >= '#{date}' AND items_users.processed is NULL " 
    # byebug
    connection = ActiveRecord::Base.connection()
    test = connection.execute(sql)
    data = test.collect{|t| t}

    total = 0
    data.each do |order|
      puts 'order no : '
      puts order["id"]
      #if order is primary
      if order["substitute"] == nil
        #check if order has substitute order
        search = "%#{order['id']}%"
        item = ItemsUser.where('substitute::text LIKE :id', :id => search )
        if item[0] != nil
          details = Item.find(item[0].item_id)
        end
        puts 'order is primary'
        puts 'price : '
        puts order["price"]*order["quantity"].to_i
        if item[0] != nil && details!= nil
          puts 'substitute price : '
          puts details.price*item[0].quantity.to_i
        end
        puts 'balance : '
        puts current_user.balance

        if item[0] != nil && details!= nil && details.price*item[0].quantity.to_i > order["price"]*order["quantity"].to_i
          total += details.price*item[0].quantity.to_i
        end
        if item[0] != nil && details!= nil && details.price*item[0].quantity.to_i < order["price"]*order["quantity"].to_i
          total += order["price"]*order["quantity"].to_i
        end
        if item[0] != nil && details!= nil && details.price*item[0].quantity.to_i == order["price"]*order["quantity"].to_i
          total += order["price"]*order["quantity"].to_i
        end
        if details == nil
          total += order["price"]*order["quantity"].to_i
        end
      #if order is substitute  
      # else
      #   item = ItemsUser.where('id'=>order["substitute"])
      #   if item[0] != nil
      #     details = Item.find(item[0].item_id)
      #   end
      #   puts 'order is substitute'
      #   puts 'price : '
      #   puts order["price"]
      #   if details!= nil
      #     puts 'substitute price : '
      #     puts details.price
      #   end
      #   puts 'balance : '
      #   puts current_user.balance

      #   if details!= nil && details.price > order["price"]
      #     total += details.price
      #   end
      #   if details!= nil && details.price < order["price"]
      #     total += order["price"]
      #   end
      #   if details == nil
      #     total += order["price"]
      #   end

      end
      puts 'sub total : '
      puts total
    end

    data.each do |order|
      if order["substitute"] != nil

        items = order["substitute"].split(",")
        puts 'found '
        puts items.count

        new_total = 0

        items.each do |i|
          item = ItemsUser.find(i)
          details = Item.find(item.item_id)
          new_total += item.quantity.to_i*details.price
        end

        puts 'new total'
        puts new_total

        details = Item.find(order["item_id"])
        
        if details.price*order["quantity"].to_i > new_total
          total -= details.price*(items.count-1)*order["quantity"].to_i
        end

      end
    end

    puts 'sub sub total : '
      puts total


      # byebug

      @item = Item.find(params[:id])

      @itemu = ItemsUser.new("user_id" => current_user.id, "item_id" => params[:id], "quantity" => params[:quant]["2"],"time_stamp" => DateTime.now.strftime("%Y-%m-%d %H:%M:%S"))
      
      @itemUser = ItemsUser.new("user_id" => current_user.id, "item_id" => params[:id], "substitute" => params[:sub],"quantity" => params[:quant]["2"], "time_stamp" => DateTime.now.strftime("%Y-%m-%d %H:%M:%S"))
      
      if params[:optradio] == "1"
        total += @item.price*params[:quant]["2"].to_i
      end

      if params[:optradio] == "2" && params[:sub] != ""
        ids = params[:sub].split(",")

        new_total = 0

        ids.each do |id|
          if id != ""
            item = ItemsUser.where('id::text LIKE :id', :id => "%#{id}%" )
            if  item.count > 0
              details = Item.find(item[0].item_id)
              new_total += details.price*item[0]["quantity"]
            end
          end
        end

        if @item.price*params[:quant]["2"].to_i > new_total
          total -= new_total
          total += @item.price*params[:quant]["2"].to_i
        end
      end

      puts 'total : '
      puts total

      
      if total <= current_user.balance
        # byebug
        if params[:optradio] == "1" && @itemu.save
          flash[:notice] = "Item Added Successfully!"
          redirect_to(:action => "order")
        end
        if params[:optradio] == "2" && params[:sub] != "" && @itemUser.save
          flash[:notice] = "Item Added Successfully!"
          redirect_to(:action => "order")
        elsif params[:optradio] == "2" && params[:sub] == ""
        flash[:alert] = "Kindly select primary order for placing substitute order!"
        redirect_to(:action => "order")
        end
      else
        flash[:alert] = "You don't have enough funds to order this item!"
        redirect_to(:action => "order")
      end
      
  end

  def delete_order
    # byebug
    @item = ItemsUser.find(params[:id])

    subitem = ItemsUser.where("substitute LIKE :substitute", :substitute => "%#{@item.id}%")
    

    if subitem[0] != nil
      a = subitem[0].substitute.gsub("#{@item.id},", "")
      if a == ""
        subitem[0].update_column :substitute, nil
      else
        subitem[0].update_column :substitute, a
      end
      
      subitem[0].save

    end

    if @item.destroy
      flash[:notice] = "Item Deleted!"
      redirect_to(:action => "order")
    end
      
  end

  def history
    #byebug
    @items = Item.sorted    

    date = Date.today.to_s
    sql = "Select * from items INNER JOIN items_users on items.id = items_users.item_id where items_users.user_id = #{current_user.id}  AND items_users.time_stamp < '#{date}' ORDER BY items_users.id DESC" 
    
    connection = ActiveRecord::Base.connection()
    test = connection.execute(sql)
    @data = test.collect{|t| t}
    # byebug
  end

  def transactions
    #byebug
    @transactions = Transaction.where("user_id" => current_user.id).order("id DESC")
  end

  def add_substitute
    @item = ItemsUser.find(params[:order])
    @item.update_column :substitute, params[:substitute]
    @item.save
    redirect_to(:action => "order")
    byebug
  end

  # def index
  #   @item = Item.sorted
  #   @item_new = Item.new

  # end

  # def show
  #   @item = Item.find(params[:id])
  # end

  # def new
  #   @item = Item.new
  # end

  # def create
  #   @item = Item.new(Item_params)
  #   if @item.save
  #     flash[:notice] = "Item Added Successfully!";
  #     @item = Item.last(1)
  #   end
    
  #   respond_to do |format|
  #     format.html{ redirect_to Item_index_path  }
  #     format.js
  #   end    
  # end

  # def edit
  #   @item = Item.find(params[:id])
  # end

  # def update
  #   @item = Item.find(params[:id])
  #   if @item.update_attributes(Item_params)
  #     flash[:notice] = "Item Details Updated!";
  #   end
    
  #   respond_to do |format|
  #     format.html{ redirect_to Item_index_path  }
  #     format.js
  #   end  

  # end

  # def delete
  #   @item = Item.find(params[:id])
  # end

  # def destroy
  #   @item = Item.find(params[:id])
  #   if @item.destroy
  #     redirect_to(:action => "index")
  #     flash[:notice] = "Item \"#{@item.name}\" Deleted!";
  #   else
  #     render("delete")
  #   end
  # end

  private
  
    def Item_params
      params.require(:Item).permit(:name,:location)
    end
end
