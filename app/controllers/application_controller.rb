class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  use_growlyflash
  
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected
  	def configure_permitted_parameters
	    added_attrs = [:name,:email,:password]
	    update_attrs = [:email,:password, :password_confrimation,:current_password,:avatar]
	    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
	   	devise_parameter_sanitizer.permit :account_update, keys: update_attrs
  	end
end
