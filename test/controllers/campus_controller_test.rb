require 'test_helper'

class CampusControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get campus_index_url
    assert_response :success
  end

  test "should get show" do
    get campus_show_url
    assert_response :success
  end

  test "should get new" do
    get campus_new_url
    assert_response :success
  end

  test "should get edit" do
    get campus_edit_url
    assert_response :success
  end

  test "should get delete" do
    get campus_delete_url
    assert_response :success
  end

  test "should get department" do
    get campus_department_url
    assert_response :success
  end

end
