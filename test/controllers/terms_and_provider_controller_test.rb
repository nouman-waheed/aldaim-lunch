require 'test_helper'

class TermsAndProviderControllerTest < ActionDispatch::IntegrationTest
  test "should get term" do
    get terms_and_provider_term_url
    assert_response :success
  end

  test "should get provider" do
    get terms_and_provider_provider_url
    assert_response :success
  end

end
